package com.Magicbean.license;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.CustomViewHolder> {
    private static final String TAG = "ProductListAdapter";

    private Context context;
    private List<Product> items;
    private int last = 0;

    public ProductAdapter(Context context, List<Product> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        final Product product = items.get(position);
        holder.gameIcon.setImageDrawable(product.getImgURL());
        holder.palyIcon.setImageDrawable(product.getdownImage());
        holder.palyIcon.setColorFilter(Color.parseColor("#002f35"), PorterDuff.Mode.SRC_IN);
        holder.gameName.setText(product.getgameName());
        holder.gameInfo.setText(product.getgameInfo());
        holder.gameInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TextUtils.isEmpty(product.getURL()))
                    return;

                Intent i = new Intent((Intent.ACTION_VIEW));
                Uri uri = Uri.parse(product.getURL());
                i.setData(uri);
                context.startActivity(i.addFlags(FLAG_ACTIVITY_NEW_TASK));
            }
        });

        holder.palyIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (Product.isInstallApp(context, "com.Magicbean." + product.getgameName().toLowerCase())) {
                        Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.Magicbean." + product.getgameName().toLowerCase());
                        //c.e.myapplication
                        //Intent intent = context.getPackageManager().getLaunchIntentForPackage("c.e.myapplication");
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    } else {
                        if(TextUtils.isEmpty(product.getdownURL()))
                            return;
                        Intent i = new Intent((Intent.ACTION_VIEW));
                        Uri uri = Uri.parse(product.getdownURL());
                        i.setData(uri);
                        context.startActivity(i.addFlags(FLAG_ACTIVITY_NEW_TASK));
                    }
                }catch (Exception e){
                    Log.d("start", e.getMessage());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView gameName;
        TextView gameInfo;
        ImageView gameIcon;
        ImageView palyIcon;
        String downURL;
        String URL;
        public CustomViewHolder(View itemView) {
            super(itemView);
            gameName = itemView.findViewById(R.id.game_name);
            gameInfo = itemView.findViewById(R.id.game_info);
            gameIcon = itemView.findViewById(R.id.game_icon);
            palyIcon = itemView.findViewById(R.id.play_icon);
        }
    }
}
