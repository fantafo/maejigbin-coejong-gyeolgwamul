package com.Magicbean.license;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.poovam.pinedittextfield.LinePinField;
import com.poovam.pinedittextfield.PinField;

import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.Magicbean.license.MainActivity.SerialKey;

public class SerialActivity extends AppCompatActivity {

    //시리얼키에대한 설명
    public static TextView explanationTextView;

    //시리얼키 입력후 나나타는 상태
    public static TextView sateTextView;

    public Context m_context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serial);

        m_context = this;
        //상세 설명 셋팅
        explanationTextView = (TextView) findViewById(R.id.SerialText);
        setStoreURL("https://smartstore.naver.com/magicbeangame");

        sateTextView = (TextView) findViewById(R.id.sateTextView);


        LinePinField linePinField = findViewById(R.id.lineField);
        if(!TextUtils.isEmpty(SerialKey) && SerialKey.length() == 9){
            linePinField.setText(SerialKey);
        }


        linePinField.setOnTextCompleteListener(new PinField.OnTextCompleteListener() {

            @Override
            public boolean onTextComplete (@NotNull String stringCode) {
                try {
                    FileOutputStream fos = openFileOutput("SerialKey.txt", Context.MODE_PRIVATE);
                    fos.write(stringCode.getBytes());
                    fos.close();
                    SerialKey = stringCode.toUpperCase();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true; // Return true to keep the keyboard open else return false to close the keyboard
            }
        });


        TextView okButton = (TextView) findViewById(R.id.ok_button) ;
        okButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (!MainActivity.RequestPermissions(SerialActivity.this, SerialActivity.this)) {
                        return;
                    }

                    if(SerialKey == null || SerialKey.length() != 9 ) {
                        sateTextView.setText("시리얼 키는 9자리 입니다...");
                        return;
                    }

                    sateTextView.setText("서버와 통신중입니다...");
                    final String uuid = DeviceUuidFactory.getDeviceUuid(SerialActivity.this, SerialActivity.this).toString();
                    String url = MainActivity.serverip + "/Home/GetLicense/";
                    ContentValues value = new ContentValues();
                    value.put("Serial", SerialKey.toUpperCase());
                    value.put("UUID", uuid);
                    NetworkTask networkTask = new NetworkTask(SerialActivity.this,SerialActivity.this, url, value, RequestHttpURLConnection.Protocol.POST);
                    networkTask.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

            String url = MainActivity.serverip + "/Home/GetStoreURL/";
            NetworkTask networkTask = new NetworkTask(this, this, url, null, RequestHttpURLConnection.Protocol.GET);
            networkTask.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        setCustomActionbar();
        //MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    public void setCustomActionbar(){
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            //layout을 가지고 와서 actionbar에 포팅을 시킵니다.
            View mCustomView = LayoutInflater.from(this).inflate(R.layout.custom_actionbar, null);
            getSupportActionBar().setCustomView(mCustomView);

            //액션바 양쪽 공백 없애기
            Toolbar parent = (Toolbar)mCustomView.getParent();
            parent.setContentInsetsAbsolute(0,0);

            this.getSupportActionBar().setCustomView(mCustomView);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.GetSN:
                SNshow();
                return true;
            /*
            case R.id.SerialKey:
                show();
                return true;
            case R.id.CreateLicense:
                if (!RequestPermissions(this, this)) {
                    return true;
                }

                if (TextUtils.isEmpty(SerialKey)) {
                    Toast.makeText(getApplicationContext(), "시리얼키가 없습니다.", Toast.LENGTH_LONG).show();
                    return true;
                }

                final String uuid = DeviceUuidFactory.getDeviceUuid(this, this).toString();
                String url=serverip + "/Home/GetLicense/";
                ContentValues value = new ContentValues();
                value.put("Serial", SerialKey);
                value.put("UUID", uuid);
                NetworkTask networkTask = new NetworkTask(this, url, value, RequestHttpURLConnection.Protocol.POST);
                networkTask.execute();

                return true;
                */
        }
        return super.onOptionsItemSelected(item);
    }

    public static void setStoreURL(String storeURL){
        String text = "시리얼 키는 MagicBean에서 구매 가능합니다.\n" +
                "구매 후, 문자를 통해 시리얼이 전달됩니다.\n" +
                //"해당 시리얼 키 9자리를 입력해주세요.\n" +
                "인증 앱 삭제 시, MagicBean 앱 이용이 불가능합니다.";
        explanationTextView.setText(text);

        Pattern pattern1 = Pattern.compile("MagicBean");
        Linkify.TransformFilter mTransform = new Linkify.TransformFilter() {
            @Override
            public String transformUrl(Matcher match, String url) {
                return "";
            }
        };
        Linkify.addLinks(explanationTextView, pattern1, storeURL ,null,mTransform);
    }




    void show() {
        final EditText edittext = new EditText(this);
        edittext.setText(SerialKey);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Serial Key Input");
        builder.setView(edittext);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SerialKey = edittext.getText().toString();
                        try {
                            FileOutputStream fos = openFileOutput("SerialKey.txt", Context.MODE_PRIVATE);
                            fos.write(SerialKey.getBytes());
                            fos.close();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getApplicationContext(), "시리얼 키 : " + SerialKey, Toast.LENGTH_LONG).show();
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.show();
    }

    void SNshow() {
        if (!MainActivity.RequestPermissions(this,this)) {
            return;
        }

        final String uuid = DeviceUuidFactory.getDeviceUuid(this, this).toString();
        if (TextUtils.isEmpty(uuid))
            return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Device SN");
        builder.setMessage(uuid);
        builder.setPositiveButton("Copy",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("SN", uuid);
                        clipboard.setPrimaryClip(clip);
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        builder.show();
    }


    public static void setLicense(Context context, String license) {
        try {
            if (TextUtils.isEmpty(license))
                return;

            FileOutputStream fos = context.openFileOutput("License.txt", Context.MODE_PRIVATE);
            fos.write(license.getBytes());
            fos.close();
        } catch (Exception e) {

        }
    }
}
