package com.Magicbean.license;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class MainActivity extends AppCompatActivity {

    public static String SerialKey;
    private RecyclerView rv;
    private LinearLayoutManager llm;
    public static List<Product> productList;
    public static ArrayList<String> URLList;

    public static String serverip = "http://13.124.22.115:80";
    //public static String serverip = "http://192.168.1.30:800";
    //서버로 url 받고 true로 변경
    private boolean isGetUrl = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CheckSerial();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            //layout을 가지고 와서 actionbar에 포팅을 시킵니다.
            View mCustomView = LayoutInflater.from(this).inflate(R.layout.custom_actionbar, null);
            getSupportActionBar().setCustomView(mCustomView);

            //액션바 양쪽 공백 없애기
            Toolbar parent = (Toolbar) mCustomView.getParent();
            parent.setContentInsetsAbsolute(0, 0);

            this.getSupportActionBar().setCustomView(mCustomView);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        setContentView(R.layout.activity_main);

        productList = new ArrayList<>();
        productList.add(new Product(this, "Yowon12", "용어가 탄생한 유래와 기능(역할).", getResources().getDrawable(R.drawable.yowon),
                "http://www.magicbeangame.com/main/product_list.php?boardT=v&page_idx=222&goods_data=aWR4PTc1Jl8mc3RhcnRQYWdlPTAmXyZsaXN0Tm89MTImXyZ0YWJsZT1yZWRfZ29vZHMmXyZwYWdlX2lkeD0yMjImXyZzZWFyY2hfaXRlbT0=||",
                "https://play.google.com/apps/internaltest/4700219591236774600"));
        productList.add(new Product(this, "GoAndStop", "컴퓨터와 프로그래머 놀이로 정확한 정보 전달하기", getResources().getDrawable(R.drawable.go_and_stop),
                "http://www.magicbeangame.com/main/product_list.php?boardT=v&page_idx=222&goods_data=aWR4PTc3Jl8mc3RhcnRQYWdlPTAmXyZsaXN0Tm89MTImXyZ0YWJsZT1yZWRfZ29vZHMmXyZwYWdlX2lkeD0yMjImXyZzZWFyY2hfaXRlbT0=||",
                "https://play.google.com/apps/internaltest/4697915171393651509"));
        productList.add(new Product(this, "Dung", "정보 이론과 검색 알고리즘", getResources().getDrawable(R.drawable.dung),
                "http://www.magicbeangame.com/main/product_list.php?boardT=v&page_idx=222&goods_data=aWR4PTc4Jl8mc3RhcnRQYWdlPTAmXyZsaXN0Tm89MTImXyZ0YWJsZT1yZWRfZ29vZHMmXyZwYWdlX2lkeD0yMjImXyZzZWFyY2hfaXRlbT0=||",
                "https://play.google.com/apps/internaltest/4699175412701969143"));
                productList.add(new Product(this, "Zip", "데이터를 압축하면 용량과 전송속도가 달라져요.", getResources().getDrawable(R.drawable.zip),
                "http://www.magicbeangame.com/main/product_list.php?boardT=v&page_idx=222&goods_data=aWR4PTgyJl8mc3RhcnRQYWdlPTAmXyZsaXN0Tm89MTImXyZ0YWJsZT1yZWRfZ29vZHMmXyZwYWdlX2lkeD0yMjImXyZzZWFyY2hfaXRlbT0=||",
                "https://play.google.com/apps/internaltest/4700174852392935328"));
        productList.add(new Product(this, "Sixteen", "데이터 구조와 정렬 알고리즘", getResources().getDrawable(R.drawable.sixteen),
                "http://www.magicbeangame.com/main/product_list.php?boardT=v&page_idx=222&goods_data=aWR4PTc5Jl8mc3RhcnRQYWdlPTAmXyZsaXN0Tm89MTImXyZ0YWJsZT1yZWRfZ29vZHMmXyZwYWdlX2lkeD0yMjImXyZzZWFyY2hfaXRlbT0=||",
                "https://play.google.com/apps/internaltest/4699302013924416766"));
        productList.add(new Product(this, "Signal", "로봇 부품 찾기 알고리즘", getResources().getDrawable(R.drawable.signal),
                "http://www.magicbeangame.com/main/product_list.php?boardT=v&page_idx=222&goods_data=aWR4PTg1Jl8mc3RhcnRQYWdlPTAmXyZsaXN0Tm89MTImXyZ0YWJsZT1yZWRfZ29vZHMmXyZwYWdlX2lkeD0yMjImXyZzZWFyY2hfaXRlbT0=||",
                "https://play.google.com/apps/internaltest/4701560804506430834"));
        productList.add(new Product(this, "Popcorn", "이진수로 맛있는 팝콘 튀기기", getResources().getDrawable(R.drawable.popcorn),
                "http://www.magicbeangame.com/main/product_list.php?boardT=v&page_idx=222&goods_data=aWR4PTg0Jl8mc3RhcnRQYWdlPTAmXyZsaXN0Tm89MTImXyZ0YWJsZT1yZWRfZ29vZHMmXyZwYWdlX2lkeD0yMjImXyZzZWFyY2hfaXRlbT0=||",
                "https://play.google.com/apps/internaltest/4697607366811015281"));
        productList.add(new Product(this, "CodeTalk", "알고리즘-순차, 조건, 반복", getResources().getDrawable(R.drawable.code_talk),
                "http://www.magicbeangame.com/main/product_list.php?boardT=v&page_idx=222&goods_data=aWR4PTgzJl8mc3RhcnRQYWdlPTAmXyZsaXN0Tm89MTImXyZ0YWJsZT1yZWRfZ29vZHMmXyZwYWdlX2lkeD0yMjImXyZzZWFyY2hfaXRlbT0=||",
                "https://play.google.com/apps/internaltest/4701038002159177541"));
        productList.add(new Product(this, "PotaStar", "스타가 되기 알고리즘은 순차, 조건, 반복", getResources().getDrawable(R.drawable.pota_star),
                "http://www.magicbeangame.com/main/product_list.php?boardT=v&page_idx=222&goods_data=aWR4PTgxJl8mc3RhcnRQYWdlPTAmXyZsaXN0Tm89MTImXyZ0YWJsZT1yZWRfZ29vZHMmXyZwYWdlX2lkeD0yMjImXyZzZWFyY2hfaXRlbT0=||",
                "https://play.google.com/apps/internaltest/4700494137813884614"));
        productList.add(new Product(this, "EggAdd", "예측할 수 없는 논리함수, 난수와 변수", getResources().getDrawable(R.drawable.egg_add),
                "http://www.magicbeangame.com/main/product_list.php?boardT=v&page_idx=222&goods_data=aWR4PTgwJl8mc3RhcnRQYWdlPTAmXyZsaXN0Tm89MTImXyZ0YWJsZT1yZWRfZ29vZHMmXyZwYWdlX2lkeD0yMjImXyZzZWFyY2hfaXRlbT0=||",
                "https://play.google.com/apps/internaltest/4701505940012399811"));
        productList.add(new Product(this, "Dosirak", "디지털 리터러시", getResources().getDrawable(R.drawable.dosirak),
                "http://www.magicbeangame.com/main/product_list.php?boardT=v&page_idx=222&goods_data=aWR4PTc2Jl8mc3RhcnRQYWdlPTAmXyZsaXN0Tm89MTImXyZ0YWJsZT1yZWRfZ29vZHMmXyZwYWdlX2lkeD0yMjImXyZzZWFyY2hfaXRlbT0=||",
                "https://play.google.com/apps/internaltest/4699901211018551563"));

        rv = (RecyclerView) findViewById(R.id.main_rv);
        rv.setHasFixedSize(true);
        llm = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(llm);
        ProductAdapter adapter = new ProductAdapter(getApplicationContext(), productList);
        rv.setAdapter(adapter);

        //서버로 url 링크 받아오기
        if(!isGetUrl) {
            String url = serverip + "/Home/GetURL/";
            NetworkTask networkTask = new NetworkTask(this, this, url, null, RequestHttpURLConnection.Protocol.GET);
            networkTask.execute();
            isGetUrl = true;
        }
    }


    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: { //toolbar의 back키 눌렀을 때 동작
                Intent introIntent = new Intent(getApplicationContext(), SerialActivity.class);
                startActivity(introIntent);
                finish();
                return true;
            }
            case R.id.GetSN:
                SNshow();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void SNshow() {
        if (!MainActivity.RequestPermissions(this, this)) {
            return;
        }

        final String uuid = DeviceUuidFactory.getDeviceUuid(this, this).toString();
        if (TextUtils.isEmpty(uuid))
            return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Device SN");
        builder.setMessage(uuid);
        builder.setPositiveButton("Copy",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("SN", uuid);
                        clipboard.setPrimaryClip(clip);
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        builder.show();
    }


    private static final int PERMISSIONS_REQUEST_CODE = 100;

    public static boolean RequestPermissions(Context context, Activity activity) {

        String[] permissions = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE};

        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permissions[0])
                || ActivityCompat.shouldShowRequestPermissionRationale(activity, permissions[1])
                || ActivityCompat.shouldShowRequestPermissionRationale(activity, permissions[2])) {
            //사용자게에 퍼미션 요청을 합니다. 요청 결과는 onRequestPermissionResult에서 수신됩니다.
            ActivityCompat.requestPermissions(activity, permissions, PERMISSIONS_REQUEST_CODE);

        } else {
            // 사용자가 퍼미션 거부를 한 적이 없는 경우에는 퍼미션 요청을 바로 합니다.
            // 요청 결과는 onRequestPermissionResult에서 수신됩니다.
            ActivityCompat.requestPermissions(activity, permissions, PERMISSIONS_REQUEST_CODE);
        }


        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                    Toast.makeText(context, " 사용권한을 활성화 해 주세요.", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        return true;
    }

    public void CheckSerial() {

        //Serial 입력값 읽기
        File file = new File(getFilesDir(), "SerialKey.txt");
        FileReader fr = null;
        BufferedReader bufrd = null;
        String str;

        if (file.exists()) {
            try {
                // open file.
                fr = new FileReader(file);
                bufrd = new BufferedReader(fr);

                if ((str = bufrd.readLine()) != null) {
                    SerialKey = str;
                }
                bufrd.close();
                fr.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        file = new File(getFilesDir(), "License.txt");
        fr = null;
        bufrd = null;
        str = "";
        String License ="";
        if (file.exists()) {
            try {
                // open file.
                fr = new FileReader(file);
                bufrd = new BufferedReader(fr);

                if ((str = bufrd.readLine()) != null) {
                    License = str;
                }
                bufrd.close();
                fr.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(License.isEmpty()){
            Intent introIntent = new Intent(getApplicationContext(), SerialActivity.class);
            startActivity(introIntent);
            finish();
        }
    }
}
