package com.Magicbean.license;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;

public class Product {
    private String gameName;
    private String gameInfo;
    private Drawable imgURL;
    private String URL;
    private Drawable downImage;
    private String downURL;
    private boolean isDown = false; //다운중, 다운완료시 ture

    public Product(Context context, String name, String info, Drawable imgURL, String URL, String downURL) {
        this.gameName = name;
        this.gameInfo = info;
        //this.imgURL = context.getResources().getDrawable(R.drawable.logo);
        this.imgURL = imgURL;
        this.URL = URL;

        if(isInstallApp(context, "com.Magicbean." + name.toLowerCase())){
            isDown = true;
            this.downImage = context.getResources().getDrawable(R.drawable.play_button);
            //downImage = ResourcesCompat.getDrawable(context.getResources(), R.drawable.logo, null);
        }else{
            //downImage = ResourcesCompat.getDrawable(context.getResources(), R.drawable.logo, null);
            this.downImage = context.getResources().getDrawable(R.drawable.down_image);
        }
        this.downURL = downURL;

    }




    public Drawable getImgURL() {
        return imgURL;
    }

    public void setImgURL(Drawable imgURL) {
        this.imgURL = imgURL;
    }

    public String getgameName() {
        return gameName;
    }

    public void setgameName(String text) {
        this.gameName = text;
    }


    public String getgameInfo() {
        return gameInfo;
    }

    public void setgameInfo(String text) {
        this.gameInfo = text;
    }


    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public Drawable getdownImage() {
        return downImage;
    }

    public void setdownImage(Drawable downImage) {
        this.downImage = downImage;
    }

    public String getdownURL() {
        return downURL;
    }

    public void setdownURL(String downURL) {
        this.downURL = downURL;
    }

    public static boolean isInstallApp(Context context, String packageName){
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);

        if(intent==null){
            //미설치
            return false;
        }else{
            //설치
            return true;
        }
    }
}
