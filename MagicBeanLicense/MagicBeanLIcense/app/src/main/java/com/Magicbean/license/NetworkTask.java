package com.Magicbean.license;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class NetworkTask extends AsyncTask<Void, Void, String> {
    private Context context;
    private Activity activity;
    private String url;
    private ContentValues values;
    private RequestHttpURLConnection.Protocol protocol;

    static void LogD(String msg) {
        Log.d("MagicBean", "[NetworkTask]" + msg);
    }
    String toBinary(byte[] bytes )
    {
        StringBuilder sb = new StringBuilder(bytes.length * Byte.SIZE);
        for(int i = 0; i < Byte.SIZE * bytes.length; i++ )
            sb.append((bytes[i / Byte.SIZE] << i % Byte.SIZE & 0x80) == 0 ? '0' : '1');
        return sb.toString();
    }

    String convertBinaryStringToString(String string){
        StringBuilder sb = new StringBuilder();
        char[] chars = string.replaceAll("\\s", "").toCharArray();
        for (int j = 0; j < chars.length; j+=8) {
            int idx = 0;
            int sum = 0;
            for (int i = 7; i>= 0; i--) {
                if (chars[i+j] == '1') {
                    sum += 1 << idx;
                }
                idx++;
            }
            System.out.println(sum);//debug
            sb.append(Character.toChars(sum));
        }
        return sb.toString();
    }

    public NetworkTask(Context context, Activity activity, String url, ContentValues values, RequestHttpURLConnection.Protocol protocol) {
        this.context = context;
        this.activity = activity;
        this.url = url;
        this.values = values;
        this.protocol = protocol;
    }

    @Override
    protected String doInBackground(Void... params) {

        String result; // 요청 결과를 저장할 변수.
        RequestHttpURLConnection requestHttpURLConnection = new RequestHttpURLConnection();
        result = requestHttpURLConnection.request(url, values, protocol); // 해당 URL로 부터 결과물을 얻어온다.

        return result;
    }

    @Override
    protected void onPostExecute(String line) {
        super.onPostExecute(line);

        try {
            if (TextUtils.isEmpty(line))
                return;
            //line = line.substring(1, line.length() - 1);
            //line = line.replaceAll("\"","");
            String[] data = line.split("=", 2);
            if (data[0].equals("license")) {
                if (TextUtils.isEmpty(data[1])) {
                    SerialActivity.sateTextView.setText("시리얼키를 확인해 주세요.");
                    return;
                }

                //라이선스키  저장
                if (data[1] != null && data[1].length() > 0) {
                    SerialActivity.setLicense(context,data[1]);
                    SerialActivity.sateTextView.setText("라이선스키 생성 완료.. \r\n3초 뒤 페이지가 이동됩니다.");
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(3000);
                                Intent introIntent = new Intent(context, MainActivity.class);
                                context.startActivity(introIntent);
                                ((Activity) context).finish();

                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }
                    }).start();
                }else {
                    SerialActivity.sateTextView.setText("라이선스키 실패.. 시리얼키를 확인해 주세요");
                }
            } else if (data[0].equals("link")) {

                Gson gson = new Gson();
                JSONArray jarray = new JSONArray(data[1]);
                ArrayList<DownLink> userList = new ArrayList<>();
                for(int i=0; i < jarray.length(); i++){
                    JSONObject jObject = jarray.getJSONObject(i);
                    String name = jObject.getString("name");
                    String url = jObject.getString("url");
                    String downurl = jObject.getString("downurl");

                    for(int j=0; j<MainActivity.productList.size(); ++j){
                        if(MainActivity.productList.get(j).getgameName().equals(name)){
                            if(!TextUtils.isEmpty(url))
                                MainActivity.productList.get(j).setURL(url);
                            if(!TextUtils.isEmpty(downurl))
                                MainActivity.productList.get(j).setdownURL(downurl);
                        }
                    }
                }

            }else if (data[0].equals("StoreURL")) {
                String url = data[1];
                SerialActivity.setStoreURL(url);
            }
        }catch (Exception e){
            e.getMessage();
            LogD(e.getMessage());
        }

        //doInBackground()로 부터 리턴된 값이 onPostExecute()의 매개변수로 넘어오므로 s를 출력한다.
        //tv_outPut.setText(s);
    }
}
