package com.Magicbean.license;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class UUIDActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uuid);

        // Create intent to deliver some kind of result data
        String Myuuid;
        String LIcense;
        String data = "";
        try {
            //Myuuid = DeviceUuidFactory.getDeviceUuid(this, this).toString();

            if (CheckLicense(this,this))
                data = "OK";
        }catch (Exception e){
            data = "";
        }
        Intent result = new Intent("com.Magicbean.license.GetUUID", Uri.parse(data));
        setResult(this.RESULT_OK, result);
        finish();
    }

    public boolean CheckLicense(Context context, Activity activity) {

        //Serial 입력값 읽기
        File file = new File(context.getFilesDir(), "License.txt");
        FileReader fr = null;
        BufferedReader bufrd = null;
        String str;
        String License = "";

        if (file.exists()) {
            try {
                // open file.
                fr = new FileReader(file);
                bufrd = new BufferedReader(fr);

                if ((str = bufrd.readLine()) != null) {
                    License = str;
                }
                bufrd.close();
                fr.close();
                String uuid = new Decryption().decrypt(License);
                String myuuid =DeviceUuidFactory.getDeviceUuid(context, activity).toString();

                if(TextUtils.isEmpty(uuid) || TextUtils.isEmpty(myuuid))
                    return false;

                if(uuid.equals(myuuid))
                    return true;

                return false;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;

    }
}
