﻿$(document).ready(function () {

    var editor = new $.fn.dataTable.Editor({

        "ajax": {
            "url": "/api/Member/Editor",
            "type": "POST",
            "datatype": 'json',
            success: function (data) {
                if (data === 'NO') {
                    alert("시도한 작업이 실패했습니다.");
                } else if (data === 'OverlapId') {
                    alert("중복된 학번입니다.");
                } else if (data === 'NoAdmin') {
                    alert("관리자 로그인이 필요합니다.");
                }
                editor.close();
                oTable = $('#demoGrid').DataTable();
                oTable.draw();
            },
            //json 형식이 아닌것이 오면 무조건 error로 가기때문에..
            error: function (response, state, errorCode) {
                editor.close();
                oTable = $('#demoGrid').DataTable();
                oTable.draw();
            }
        },

        table: "#demoGrid",
        idSrc: 'Serial',
        fields:
            [
                {
                    label: "구매자 정보:",
                    name: "UserInfo",

                },
                {
                    label: "시리얼:",
                    name: "Serial",
                },
                {
                    label: "구매 날짜:",
                    name: "Date",
                },
                {
                    label: "구매 유형:",
                    name: "Purchase",
                },
                {
                    label: "사용 여부",
                    name: "isUse"
                }
                /*
                {
                    label: "지도 교수:",
                    name: "Professor",
                },
                {
                    label: "성별:",
                    name: "Gender",
                    type: "select",
                    options: [
                        { label: "남성", value: "0" },
                        { label: "여성", value: "1" },
                    ]
                }
                */
            ],
        i18n: {
            edit: {
                title: "학생 정보 수정",
                submit: "수정"
            },
            remove: {
                title: "학생 정보 삭제",
                submit: "삭제",
                confirm: {
                    _: "%d명의 학생 정보를 삭제하겠습니까?",
                    1: "1명의 학생 정보를 삭제하겠습니까?"
                }
            },
        }

    });

    var table = $("#checkSerialTable").DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        //dom: 'frtip',

        buttons: [
            {
                extend: "edit",
                editor: editor,
                text: "수정"
            },

            {
                extend: "remove",
                editor: editor,
                text: "삭제",
                /*
                action: function () {
 
                    //DeleteData(idSrc);
                }
                */
            },

            {
                extend: 'print',
                customizeData: function () {
                    option = false;
                },
                text: '프린트',
                exportOptions: {
                    columns: [1, 2, 3]
                }
            },

            {
                extend: 'excelHtml5',

                customizeData: function (data) {
                    option = false;
                },
                text: '정보 Excel 저장',
                title: 'VR 안전교육 학생 정보',
                exportOptions: {
                    columns: [1, 2, 3]
                },
            },
        ],

        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "pageLength": 10,
        "select": 'multi',
        order: [2, 'dsc'],
        "language": {
            "lengthMenu": " 표_MENU_",
            "search": "검색",
            select: {
                rows: {
                    _: '%d rows selected',
                    0: '선택하여 수정/삭제 가능합니다',
                    1: ''
                }
            },
            info: "검색결과 _TOTAL_명",
            infoEmpty: "검색결과 0명",
            paginate: {
                "first": "First",
                "last": "Last",
                "next": "다음",
                "previous": "이전"
            },

        },
        lengthMenu: [
            [10, 25, 50, -1],
            ['10', '25', '50', 'All']
        ],

        "ajax": {
            "url": "/LMS/CheckSerialLoadData",
            "type": "POST",
            "datatype": "json"
        },

        "columnDefs":
            [

            ],

        "columns": [
            { "data": "Info", "name": "Info" },
            { "data": "Serial", "name": "Serial", "autoWidth": true },
            { "data": "Date", "name": "Date", "autoWidth": true },
            { "data": "Purchase", "name": "Purchase", "autoWidth": true }
        ]

    });



});