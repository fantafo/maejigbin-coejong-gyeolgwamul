﻿$(document).ready(function () {

    var editor = new $.fn.dataTable.Editor({

        "ajax": {
            "url": "/api/Serial/CompaniesEditor",
            "type": "POST",
            "datatype": 'json',
            success: function (data) {
                editor.close();
                oTable = $('#companiesTable').DataTable();
                oTable.draw();
            },
            //json 형식이 아닌것이 오면 무조건 error로 가기때문에..
            error: function (response, state, errorCode) {
                editor.close();
                oTable = $('#companiesTable').DataTable();
                oTable.draw();
            }
        },

        table: "#companiesTable",
        idSrc: 'Serial',
        fields:
            [
                {
                    label: "CompaniesInfo",
                    name: "CompaniesInfo"
                },
                {
                    label: "Serial",
                    name: "Serial",
                    type: "hidden",
                },
            ],
        i18n: {
            edit: {
                title: "기업 정보 수정",
                submit: "수정"
            },
            remove: {
                title: "기업 정보 삭제",
                submit: "삭제",
                confirm: {
                    _: "%d개의 기업 시리얼을 삭제하겠습니까?",
                    1: "기업 시리얼을 삭제하겠습니까?"
                }
            },
        }

    });

    var table = $("#companiesTable").DataTable({
        dom: '<"html5buttons"B>Tfgitp',
        //dom: 'frtip',

        buttons: [
            {
                extend: "edit",
                editor: editor,
                text: "수정"
            },

            {
                extend: "remove",
                editor: editor,
                text: "삭제"
            }
        ],

        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "pageLength": 10,
        select: {
            style: 'os',
            selector: 'td:not(:first-child)'
        },
        order: [2, 'dsc'],
        "language": {
            "lengthMenu": " 표_MENU_",
            "search": "검색",
            select: {
                rows: {
                    _: '%d rows selected',
                    0: '선택하여 수정/삭제 가능합니다',
                    1: ''
                }
            },
            info: "검색결과 _TOTAL_명",
            infoEmpty: "검색결과 0명",
            paginate: {
                "first": "First",
                "last": "Last",
                "next": "다음",
                "previous": "이전"
            },

        },
        lengthMenu: [
            [10, 25, 50, -1],
            ['10', '25', '50', 'All']
        ],

        "ajax": {
            "url": "/LMS/CompaniesLoadData",
            "type": "POST",
            "datatype": "json"
        },

        "columnDefs":
            [

            ],

        "columns": [
            { "data": "CompaniesInfo", "name": "CompaniesInfo" },
            { "data": "Serial", "name": "Serial", "autoWidth": true },
            { "data": "Date", "name": "Date", "autoWidth": true },
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            { "data": "Count", "name": "Count", "autoWidth": true }
        ]

    });

    // Add event listener for opening and closing details
    $('#companiesTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            destroyChild(row);
            tr.removeClass('shown');
        }
        else {
            // Open this row
            createChild(row, 'child-table'); // class is for background colour
            tr.addClass('shown');
        }
    });

});

function createChild(row) {
    // This is the table we'll convert into a DataTable
    var table = $('<table class="display" width="100%"/>');

    // Display it the child row
    row.child(table).show();

    var rowdata = row.data().LicenseList;

    var rowName = row.data().CompaniesInfo;

    // Initialise as a DataTable
    var usersTable = table.DataTable({
        dom: '<"html5buttons"B>Tgitp',
        buttons: [
            {
                extend: 'csv',
                charset: 'UTF-8',
                fieldSeparator: ',',
                bom: true,
                filename: rowName + '세부 정보',
                text: 'CSV 저장'
            }
        ],

        "language": {
            "lengthMenu": " 표_MENU_",
            "search": "검색",
            info: "검색결과 _TOTAL_명",
            infoEmpty: "검색결과 0명",
            paginate: {
                "first": "First",
                "last": "Last",
                "next": "다음",
                "previous": "이전"
            },

        },
        data: rowdata,
        columns: [
            {
                title: '발급 날짜', "data": "licenseKeyIssueDate", "name": "licenseKeyIssueDate", "autoWidth": true,
                render: function (data, type, row) {
                    if (type === "sort" || type === "type") {
                        return data;
                    }
                    return moment(data).format("YYYY-MM-DD HH:mm");
                }
            },
            { title: '기기 고유 번호', "data": "UDID", "name": "UDID", "autoWidth": true},
            { title: '라이선스 키', "data": "licenseKey", "name": "licenseKey", "autoWidth": true},
        ]
    });
}
function destroyChild(row) {
    var table = $("table", row.child());
    table.detach();
    table.DataTable().destroy();

    // And then hide the row
    row.child.hide();
}
