﻿$(document).ready(function () {
    var editor = new $.fn.dataTable.Editor({

        "ajax": {
            "url": "/api/Serial/PersonalEditor",
            "type": "POST",
            "datatype": 'json',
            success: function (data) {
                editor.close();
                oTable = $('#personalTable').DataTable();
                oTable.draw();
            },
            //json 형식이 아닌것이 오면 무조건 error로 가기때문에..
            error: function (response, state, errorCode) {
                editor.close();
                oTable = $('#personalTable').DataTable();
                oTable.draw();
            }
        },

        table: "#personalTable",
        idSrc: 'Serial',
        fields:
            [
                {
                    label: "UserInfo",
                    name: "UserInfo"
                },
                {
                    label: "Serial",
                    name: "Serial",
                    type: "hidden",
                },
            ],
        i18n: {
            edit: {
                title: "개인 사용자 정보 수정",
                submit: "수정"
            },
            remove: {
                title: "개인 사용자 정보 삭제",
                submit: "삭제",
                confirm: {
                    _: "%d개의 개인 사용자 시리얼을 삭제하겠습니까?",
                    1: "개인 사용자 시리얼을 삭제하겠습니까?"
                }
            },
        }

    });

    var table = $("#personalTable").DataTable({
        dom: '<"html5buttons"B> Tfgitp',
        //dom: '<"html5buttons"B>lTfgitp ',
        //dom: 'frtip',
        buttons: [
            {
                extend: "edit",
                editor: editor,
                text: "수정"
            },

            {
                extend: "remove",
                editor: editor,
                text: "삭제",
                /*
                action: function () {
 
                    //DeleteData(idSrc);
                }
                */
            },
        ],

        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "pageLength": 10,
        "select": 'single',
        order: [2, 'dsc'],
        "language": {
            "lengthMenu": " 표_MENU_",
            "search": "검색",
            select: {
                rows: {
                    _: '%d rows selected',
                    0: '선택하여 수정/삭제 가능합니다',
                    1: ''
                }
            },
            info: "검색결과 _TOTAL_명",
            infoEmpty: "검색결과 0명",
            paginate: {
                "first": "First",
                "last": "Last",
                "next": "다음",
                "previous": "이전"
            },

        },
        lengthMenu: [
            [10, 25, 50, -1],
            ['10', '25', '50', 'All']
        ],

        "ajax": {
            "url": "/LMS/LoadData",
            "type": "POST",
            "datatype": "json"
        },

        "columnDefs":
            [
                
            ],

        "columns": [
            { "data": "UserInfo", "name": "UserInfo" },
            { "data": "Serial", "name": "Serial", "autoWidth": true },
            { "data": "Date", "name": "Date", "autoWidth": true },
            { "data": "Purchase", "name": "Purchase", "autoWidth": true },
            { "data": "isUse", "name": "isUse"}
        ]

    });



});